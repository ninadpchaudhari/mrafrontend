// JavaScript Document
"use strict";

var allViews = ["athleteEntry", "loginFormContainer", "adminPanel"];
var backendURL = "http://localhost:8000";
var user = {};
var credentials = {
    email: "",
    password: ""
};
var user = {};
var competitions = {};

var currentCompetitionId = "";
var currentView = "";

function setUser() {
    console.log("calling setUser");
    return $.ajax({
        type: "POST",
        url: backendURL + "/api/me",
        data: {},
        dataType: 'json',
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        success: function (data) {
            user = data.user;
            return true;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed POST /me: " + textStatus + " " + errorThrown);
            if (XMLHttpRequest.status.toString() === '401'.toString()) {
                console.log("Me Callback 401 Detected");
                $("#loginFormContainer").trigger("init");
            }
            console.log("Some error orrured while doing /me call");
            return false;
        },
        timeout: 3000,
        async: false
    });
}

$(document).ready(function () {
    $.each(allViews, function (index, view) {
        $("#" + view).hide();
    });

    //$("#loginFormContainer").trigger("init");
    //Doing this for Developing the Admin interface
    credentials.email = "ninadpchaudhari@gmail.com";
    credentials.password = "NINAD111";

    //$("#loginForm").show();
    $("#navBar").trigger("init");
    //$("#loginForm").trigger("init");
    //$("#loginForm").trigger("init");
    //$("#athleteEntry").trigger("init");
});
$("#navBar").on("init", function () {
    fetchCompetitions(function (competitions) {
        $.each(competitions, function (key, competition) {
            $("#matchSelect").append('<option value="' + competition.id + '">' + competition.name + " " + competition.year + '</option>');

        });
        $("#matchSelect option").last().prop('selected', true);
        currentCompetitionId = $("#matchSelect option").last().val();
        $(".matchSelect").chosen();
        console.log("triggeing init on athleteEntry");
        $("#athleteEntry").trigger("init");
    });
});
$("#matchSelect").on('change', function () {

    currentCompetitionId = $(this).val();
});
$("#athleteEntry").on("init", function () {
    $("#currentView").html($("#athleteEntry"));
    $("#athleteEntry").show();
    $(".athleteGender").chosen();
    fetchUnits(function (units) {
        console.log(units);
        $.each(units, function (key, unit) {
            $("#athleteUnit").append('<option value="' + unit.id + '">' + unit.name + '</option>');
        });
        $(".athleteUnit").chosen();
        $(".athleteUnit").trigger("chosen:updated");
    });
    fetchEvents(function (events) {

        $.each(events, function (key, event) {

            $("#eventsSelector").append('<option value="' + event.id + '">' + event.name + " " + event.display_name + '</option>');

        });
        $(".eventsSelector").css({
            "width": "350px"
        });
        $(".eventsSelector").chosen();
        $(".eventsSelector").trigger("chosen:updated");
    });
    /*
    $("#previousEntriesGrid").jsGrid({
        width: "100%",
        height: "400px",
        autoload:true,
        controller:{
            loaddata: function(filter){
                console.log("I am here");
                
                return $.ajax({
                    type: "GET",
                    url: backendURL + "/api/entries/competition_id/" + currentCompetitionId,
                    dataType: "json",
                    headers: {
                        "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
                    }
                })
            }
        },
        noDataContent: "No Record Found",
        loadIndication: true,
        loadIndicationDelay: 500,
        loadMessage: "Please, wait...",
        loadShading: true,
        
        sorting: true,
        paging: true,
        fields:[
            {name: "Cptr_No", type: "number",width: 50},
            {name: "Name", type: "text",width: 150},
            {name: "Events", type: "text",width: 100},
        ]
    });
    */


    $(".eventsSelector").trigger("chosen:updated");
    $("#previousEntriesTable").trigger("refresh");
});
$("#loginForm").on("submit", function (event) {
    event.preventDefault();
    tryLogin(setUser().then(function () {
        $("#navBar").trigger("init");
        //$("#adminPanel").trigger("init");
        $("#athleteEntry").trigger("init");
    }));
    console.log("triggring init on adminPanel");


});
$("#adminPanel").on("init", function () {
    $("#currentView").html($("#adminPanel"));
    $("#adminPanel").show();
    fetchCompetitions(function (competitions) {
        $.each(competitions, function (key, competition) {
            $("#competitionsTable > tbody").append(createCompetitionTableRow(competition));
        });
    });


});
$("#previousEntriesTable").on("refresh", function () {
    fetchPreviousEntries(function (entries) {
        $("#previousEntriesTable > tbody").html("");
        $.each(entries, function (key, entry) {
            $("#previousEntriesTable > tbody").append(createPreviousEntriesTableRow(entry));
        });
    });
});
$("#competitionsTable").on("click", "tbody tr", function (event) {

    var id = $(this).find("#competitionID").text();

    $("#competitionActionsSelector").trigger("init", id);
    event.preventDefault();

});
$("#competitionActionsSelector").on("init", function (event, id) {
    fetchCompetitionDetails(id, function (competition) {
        //alert(output);
        alert(competition.name);
        $("#actionName").text(competition.name);
        $("#actionFullName").text(competition.display_name);
        $("#actionYear").text(competition.year);

    });
    $(this).find(".panel-body").append(id);
});

function createCompetitionTableRow(competition) {
    return `<tr>
            <td id="competitionID">${competition.id}</td>
            <td>${competition.name}</td>
            <td>${competition.year}</td>
         </tr>`
}

function createPreviousEntriesTableRow(previousEntry) {
    return `<tr>
            <td id="previousEntryID">${previousEntry.id}</td>
            <td>${previousEntry.name}</td>
            <td>${previousEntry.participated_events}</td>
         </tr>`
}
$("#newAthleteForm").on("submit", function (e) {
    e.preventDefault();
    newEntrySave(function () {
        $("#newAthleteForm").trigger("reset");
        $("#eventsSelector").val('').trigger("chosen:updated");
        $("#previousEntriesTable").trigger("refresh");
    });
    return false;
});
$("#loginFormContainer").on("init", function () {
    console.log("Loading LoginForm container");
    $("#currentView").html($("#loginFormContainer"));
    $("#loginFormContainer").show();
    $("#loginForm [name=email]").focus();
});
$("#manageAthletesButton").on("click", function () {

});


function tryLogin(nextFunction) {
    $.ajax({
        type: "POST",
        url: backendURL + "/api/me",
        dataType: 'json',
        headers: {
            "Authorization": "Basic " + btoa($("#loginForm [name=email]").val() + ":" + $("#loginForm [name=password]").val())
        },
        success: function (data) {

            console.log("Logged in");

            //$("#log").html(data.token);


            $("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-success");
            $("#loginFormContainer .panel .panel-title").text("Login Successful");
            credentials.email = data.user.email;
            credentials.password = $("#loginForm [name=password]").val();
            $("#loginForm [name=password]").val("");
            setUser();

            if (nextFunction) {
                nextFunction();
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed: " + textStatus + " " + errorThrown);
            if (XMLHttpRequest.status.toString() === '401'.toString()) {
                console.log("401 Detected");
                $("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-danger");
                $("#loginFormContainer .panel .panel-title").text("Incorrect Credentials. Login Again");


            } else {
                $("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-danger");
                $("#loginFormContainer .panel .panel-title").text("Unknown Error Login Again");
                console.log("unknown error : " + errorThrown);
            }
        },
        timeout: 3000,
        async: false

    });
}

function fetchCompetitions(handleData) {
    console.log("calling fetchCompetitions");
    return $.ajax({
        type: "GET",
        url: backendURL + "/api/competitions",
        data: {},
        dataType: 'json',
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        success: function (data) {
            handleData(data.competitions);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed GET /competitions: " + textStatus + " " + errorThrown);
            if (XMLHttpRequest.status.toString() === '401'.toString()) {
                console.log("Competitions Callback 401 Detected");
                $("#loginFormContainer").trigger("init");
            }
            console.log("Some error orrured while doing /competitions call");
            return false;
        },
        timeout: 3000,
    });
}

function fetchCompetitionDetails(id, handleData) {
    console.log("calling fetchCompetitionsDetails");

    return $.ajax({
        type: "GET",
        url: backendURL + "/api/competitions/" + id,
        data: {},
        dataType: 'json',
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        success: function (data) {
            handleData(data.competition);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed GET /competitions/id: " + textStatus + " " + errorThrown);
            if (XMLHttpRequest.status.toString() === '401'.toString()) {
                console.log("competitions id Callback 401 Detected");
                $("#loginFormContainer").trigger("init");
            }
            console.log("Some error orrured while doing /competitions id call");
            return false;
        },
        timeout: 3000,
    });
}

function fetchEvents(nextFunction) {
    return $.ajax({
        type: "GET",
        url: backendURL + "/api/events?competition_id=" + currentCompetitionId,
        dataType: "json",
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        success: function (data) {

            if (nextFunction)
                nextFunction(data.events);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed GET /api/events?competition_id= " + currentCompetitionId + textStatus + " " + errorThrown);

            console.log("Some error orrured while doing /api/events?competition_id= " + currentCompetitionId);
            return false;
        },
    });
}

function fetchUnits(nextFunction) {
    return $.ajax({
        type: "GET",
        url: backendURL + "/api/units",
        dataType: "json",
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        success: function (data) {

            if (nextFunction)
                nextFunction(data.units);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed GET /api/units" + currentCompetitionId + textStatus + " " + errorThrown);

            console.log("Some error orrured while doing /api/units" + currentCompetitionId);
            return false;
        },
    });
}

function newEntrySave(nextFunction) {
    return $.ajax({
        type: "POST",
        url: backendURL + "/api/internalEntryForm",
        dataType: "json",
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        data: {
            competition_id: currentCompetitionId,
            athleteName: $("#athleteName").val(),
            athleteUnit: $("#athleteUnit").val(),
            qualificationScore: $("qualificationScore").val(),
            athleteAddress: $("#athleteAddress").val(),
            athleteDOB: $("#athleteDOB").val(),
            athleteContact: $("#athleteContact").val(),
            athleteGender: $("#athleteGender").val(),
            eventsSelector: $("#eventsSelector").val()
        },
        success: function (data) {

            if (nextFunction)
                nextFunction(data.units);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed POST /api/internalEntryForm" + textStatus + " " + errorThrown);

            console.log("Some error orrured while doing /api/internalEntryForm");
            return false;
        },
    });
}

function fetchPreviousEntries(nextFunction) {
    return $.ajax({
        type: "GET",
        url: backendURL + "/api/entries/competition_id/" + currentCompetitionId,
        dataType: "json",
        headers: {
            "Authorization": "Basic " + btoa(credentials.email + ":" + credentials.password)
        },
        success: function (data) {

            if (nextFunction)
                nextFunction(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#log").append("failed GET /api/entries/competition_id/" + currentCompetitionId + textStatus + " " + errorThrown);

            console.log("Some error orrured while doing /api/entries/competition_id/" + currentCompetitionId);
            return false;
        },
    });
}