// JavaScript Document
"use strict";

var allViews = [ "athleteEntry", "loginFormContainer", "adminPanel" ];
var backendURL = "http://localhost:8000";
var token;
var lastStateCheckTime = 0;

function doLogin(pToken,callback){
		token = pToken;
		refreshToken().then(setUser().then(callback));
		//refreshToken(function(callback){setUser(callback);});
		
}
var user = {};
function refreshToken(){
	var dateObj = new Date();
		//time since epoch in sec
	var currentTime = Math.round(dateObj.getTime() / 1000);
		if(currentTime - lastStateCheckTime >= 120){
			//Time is more than 2 min
			
			lastStateCheckTime = currentTime;
			return doRefreshToken();
			
		}
		
		else {
			
			return $.Promise(); }
}

function doRefreshToken(){
	console.log("calling doRefreshTOken");
		if(token){
			$.ajax({
        type: "POST",
			beforeSend: function(request){
				
				console.log("before send deRefreshToken Token" + token );
				request.setRequestHeader("Authorization", "Bearer " + token);
				
			},
        url: backendURL+ "/api/refresh",
    	success: function(data){
			//console.log("doStateCheck sucess refresh data.token : "+ data.token);
			token = data.token;
			
			return $.Promise();
		},
				//calling the login form if check finds that the token is not valid
		error: function(XMLHttpRequest, textStatus, errorThrown){
			$("#log").append("failed POST /refresh: " + textStatus + " "+ errorThrown);
			if(XMLHttpRequest.status.toString() === '401'.toString()){
				console.log("401 Detected");
				$("#loginFormContainer").trigger("init");
				$("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-danger");
				$("#loginFormContainer .panel .panel-title").text("Token refresh 401 Error Please Login");
			}
			else{
				$("#loginFormContainer").trigger("init");
				$("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-danger");
				$("#loginFormContainer .panel .panel-title").text("Token Refresh unknown error Please Login");
			}
			console.log("Refresh failed : " + textStatus);
			callback(-1);
		},
		timeout: 3000
		
	});
		}
	else {
		console.log("Not refreshing because token : ");
	}
}
function setUser() {
	console.log("calling setUser");
		$.ajax({
        type: "POST",
			beforeSend: function(request){
				request.setRequestHeader("Authorization", "Bearer " + token);	
			},
        url: backendURL+ "/api/me",
		data: {},
    	success: function(data){
			console.log("Me call from login object successed");
			user = data.user;
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown,callback){
			$("#log").append("failed POST /me: " + textStatus + " "+ errorThrown);
			if(XMLHttpRequest.status.toString() === '401'.toString()){
				console.log("401 Detected");
				$("#loginFormContainer").trigger("init");
			}
			callback(-1);
		},
		timeout: 3000
		
	});
	}
	
	
$(document).ready(function(){
    $.each(allViews,function(index,view){
        $("#" + view).hide();
    });
    
    $("#loginFormContainer").trigger("init");
    
});
$("#loginForm").on("submit",function(event){
	event.preventDefault();
	tryLogin(function(){
		console.log("triggring init on adminPanel");
		$("#adminPanel").trigger("init");
	});
	
});
$("#adminPanel").on("init",function(){
		$("#currentView").html($("#adminPanel"));
		$("#adminPanel").show();
	
	
});
$("#loginFormContainer").on("init",function(event){
	$("#currentView").html($("#loginFormContainer"));
	$("#loginFormContainer").show();
    $("#loginForm [name=email]").focus();
});

function tryLogin(nextFunction){
    $.ajax({
        type: "POST",
        url: backendURL+ "/api/login",
		data: {email: $("#loginForm [name=email]").val(), password: $("#loginForm [name=password]").val()},
    	success: function(data){
			
			console.log("Logged in");
			
			//$("#log").html(data.token);
			$("#loginForm [name=password]").val("");
			
			$("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-success");
			$("#loginFormContainer .panel .panel-title").text("Login Successful");
			doLogin(data.token,nextFunction);
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			$("#log").append("failed: " + textStatus + " "+ errorThrown);
			if(XMLHttpRequest.status.toString() === '401'.toString()){
				console.log("401 Detected");
				$("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-danger");
				$("#loginFormContainer .panel .panel-title").text("Incorrect Credentials. Login Again");
				
				
			}else {
				$("#loginFormContainer .panel").removeClass("panel-default").addClass("panel-danger");
				$("#loginFormContainer .panel .panel-title").text("Unknown Error Login Again");
			}
		},
		timeout: 3000
		
	});
}
function fetchEvents(){
    $.ajax({
        type: "GET",
        url: "http://mra.dev/json/event/1",
        dataType: "html",
    }).done(function(obj){
            obj = JSON.parse(obj);
            $.each(obj, function(i, data) {
                $("#eventsSelector").append('<option value="' + data.id + '">' + data.class + '</option>');
            });
        
        $('.eventsSelector').chosen({width: "350px"});
        
    }).fail(function(){
        $("#log").text("failed");
    }).always(function(){
        $("#log").append("ran");
    });
}